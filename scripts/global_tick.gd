extends Node

enum state{
	stop
	run
}
export (state) var current_state = state.run
var max_minute = 0 
var max_second = 20
export (int) var minutes = max_minute
export (int) var seconds = max_second
var last_second = OS.get_time().second
# Called every frame. 'delta' is the elapsed time since the previous frame.
func decrease_remaining_time():
	if(seconds == 0):
		seconds=60
		minutes=minutes-1
	if(minutes == -1):
		minutes = max_minute
		seconds = max_second
		current_state = state.stop
		current_state = state.stop
	seconds = seconds -1
	if(minutes == 0  && seconds == 0): #fake trigger
		get_tree().change_scene("res://scenes/terminal.tscn")
func restart():
	current_state = state.run
	
func _process(_delta):
	var now = OS.get_time()
	if (current_state == state.run && now.second !=last_second):
		decrease_remaining_time()
		last_second = now.second
