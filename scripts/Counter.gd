extends Control

func _process(delta):
	$label.text= str(GlobalTick.minutes).pad_zeros(2) + str(":") + str(GlobalTick.seconds).pad_zeros(2)
	if GlobalTick.minutes == 0:
		$label.set("custom_colors/font_color", Color(1,0.2,0.2))
