extends KinematicBody2D

export (int) var speed = 200

var target   = Vector2()
var velocity = Vector2()

func _input(event):
	if event.is_action_pressed('l_click'):
		target = get_global_mouse_position()

func handle_collisions(collider_name):
	if(collider_name == "door_left_up"):
		get_tree().change_scene("res://scenes/gym.tscn")
	elif (collider_name == "door_right_down"):
		get_tree().change_scene("res://scenes/main_room.tscn")
	else:
		print(collider_name)
func _physics_process(delta):
	velocity = (target - position).normalized() * speed
	# rotation = velocity.angle()
	if (target - position).length() > 5:
		velocity = move_and_slide(velocity)
		for i in get_slide_count():
			var collision = get_slide_collision(i)
			handle_collisions(collision.collider.name)
